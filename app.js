const express = require('express')
const app = express()
var path = require('path')

app.use(express.static(__dirname + '/public'));
app.use(express.static('./public'));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/index', function (req, res) {
    const nbColumns = Number(req.query.nbColumns);
    const nbRows = Number(req.query.nbRows);
    let errors = [];

    if (isNaN(nbColumns) || isNaN(nbRows)){
      errors.push('ERROR : Invalid number columns/rows ')
    }

    if (nbRows === 0 || nbColumns === 0  ){
        errors.push('ERROR : Please enter a number greater or equal to 1')
    }

    res.render('pages/index', {nbColumns, nbRows, errors})
  })

  app.get('*', function(req, res) {
    res.redirect('/index');
  });

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server listenning on port ${PORT}`));
